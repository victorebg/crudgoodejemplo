﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tweetbook.Contracts.V1;
using Tweetbook.Contracts.V1.Requests;
using Tweetbook.Contracts.V1.Responses;
using Tweetbook.Domain;
using Tweetbook.Services;
using Microsoft.AspNetCore.Http;
using Tweetbook.Extensions;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Tweetbook.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PostsController : Controller
    {

        private readonly IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        [Route(ApiRoutes.Posts.GetAll)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _postService.GetPostAsync());
        }

        [Route(ApiRoutes.Posts.Create)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreatePostRequest postRequest)
        {
            var postId = new Guid();
            var post = new Post
            {
                Id = postId,
                Name = postRequest.Name,
                UserId = HttpContext.GetUserId(),
                //Tags = postRequest.Tags.Select(x => new PostTag { PostId = postId, TagName = x }).ToList()
            };

            var created = await _postService.CreatePostAsync(post);

            if (!created)
                return BadRequest();

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUrl = baseUrl + "/" + ApiRoutes.Posts.Get.Replace("{postId}", post.Id.ToString());

            var response = new CreatePostResponse { Id = post.Id };
            return Created(locationUrl, post);
        }

        [Route(ApiRoutes.Posts.Get)]
        [HttpGet]
        public async Task<IActionResult> Get([FromRoute] Guid postId)
        {
            var post = await _postService.GetPostByIdAsync(postId);
            return Ok(post);

        }

        [Route(ApiRoutes.Posts.Delete)]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] Guid postId)
        {
            var userOwnsPost = await _postService.UserOwnsPostAsync(postId, HttpContext.GetUserId());

            if (!userOwnsPost)
            {
                return BadRequest(new { Error = "You do not own this post" });
            }
            var deleted = await _postService.DeletePostAsync(postId);
            if (deleted)
                return Ok();

            return NotFound();

        }

        [Route(ApiRoutes.Posts.Update)]
        [HttpPut]
        public async Task<IActionResult> Update([FromRoute] Guid postId, [FromBody] UpdatePostRequest postRequest)
        {
            var userOwnsPost = await _postService.UserOwnsPostAsync(postId, HttpContext.GetUserId());

            if (!userOwnsPost)
            {
                return BadRequest(new { Error = "You do not own this post" });
            }

            var post = new Post
            {
                Id = postId,
                Name = postRequest.name
            };

            var updated = await _postService.UpdatePostAsync(post);

            if (updated)
                return Ok(post);

            return NotFound();

        }
    }
}
