﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tweetbook.Domain
{
    public class PostTag
    {
        [Key]
        public Guid Id { get;set ;}

        public Guid PostId { get; set; }
        public string TagName { get; set; }

      //  [ForeignKey(nameof(TagName))]
       // public virtual Tag Tags { get; set; }

        public virtual Post Posts { get; set; }

    }
}
