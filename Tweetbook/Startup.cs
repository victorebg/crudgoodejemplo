﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tweetbook.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tweetbook.Configurations;
using Swashbuckle.AspNetCore.Swagger;
using Tweetbook.Installers;

namespace Tweetbook
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            InstallerExtensions.InstallServicesInAssembly(services,Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
           
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            var swaggerConfig = new SwaggerConfigurations();
            Configuration.GetSection(nameof(SwaggerConfigurations)).Bind(swaggerConfig);

            app.UseSwagger(config =>
            {
                config.RouteTemplate = swaggerConfig.JsonRoute;
            });

            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint(swaggerConfig.UIEndpoint, swaggerConfig.Description);
            });
            
            app.UseMvc();
        }
    }
}
