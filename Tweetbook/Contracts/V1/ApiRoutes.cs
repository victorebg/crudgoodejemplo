﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tweetbook.Contracts.V1
{
    public static class ApiRoutes
    {
        private const string Root = "api";
        private const string Version = "v1";
        private const string Base = Root + "/" + Version;

        public static class Posts
        {
            public const string GetAll = Base + "/posts";
            public const string Create = Base + "/posts";
            public const string Get = Base + "/{postId}";
            public const string Delete = Base + "/{postId}";
            public const string Update = Base + "/{postId}";
        }

        public static class Tags
        {
            public const string GetAll = Base + "/tags";
            public const string Create = Base + "/tags";
            public const string Get = Base + "/{nameTag}";
            public const string Delete = Base + "/{nameTag}";
        }

        public static class Identity
        {
            public const string Login = Base + "/identity/login";
            public const string Registro = Base + "/identity/register";
            public const string RefreshToken = Base + "/identity/refreshToken";
        }
    }
}
