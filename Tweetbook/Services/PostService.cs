﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tweetbook.Data;
using Tweetbook.Domain;

namespace Tweetbook.Services
{
    public class PostService : IPostService
    {
        private readonly DataContext _dataContext;
        
        private readonly ITagService _tagService;
        public PostService(DataContext dataContext, ITagService tagService)
        {
            _dataContext = dataContext;
            _tagService = tagService;
        }

        public async Task<bool> CreatePostAsync(Post post)
        {
            await _dataContext.Posts.AddAsync(post);
          //  await _tagService.CreateTagsListAsync(post.Tags);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }


        public async Task<bool> DeletePostAsync(Guid Id)
        {
            var post = await GetPostByIdAsync(Id);
            if (post == null)
                return false;

            _dataContext.Posts.Remove(post);
            var deleted = await _dataContext.SaveChangesAsync();

            return deleted > 0;
        }


        public async Task<List<Post>> GetPostAsync()
        {
            return await _dataContext.Posts.ToListAsync();
        }

        public async Task<Post> GetPostByIdAsync(Guid Id)
        {
            return await _dataContext.Posts
               // .Include(x => x.Tags)
                .SingleOrDefaultAsync(x => x.Id == Id);
        }

    

        public async Task<bool> UpdatePostAsync(Post postToUpdate)
        {
            _dataContext.Posts.Update(postToUpdate);
            
        //    await _tagService.CreateTagsListAsync(postToUpdate.Tags);
            var updated = await _dataContext.SaveChangesAsync();

            return updated > 0;
        }

        public async Task<bool> UserOwnsPostAsync(Guid postId, string userId)
        {

            var post = await _dataContext.Posts.AsNoTracking().SingleOrDefaultAsync(x => x.Id == postId);

            if (post == null)
            {
                return false;
            }

            if (post.UserId != userId)
            {
                return false;
            }

            return true;
        }
        

    }
}
