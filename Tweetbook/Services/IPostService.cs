﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tweetbook.Domain;

namespace Tweetbook.Services
{
    public interface IPostService
    {
        Task<List<Post>> GetPostAsync();
        Task<Post> GetPostByIdAsync(Guid Id);
        Task<bool> CreatePostAsync(Post post);
        Task<bool> DeletePostAsync(Guid Id);
        Task<bool> UpdatePostAsync(Post post);
        Task<bool> UserOwnsPostAsync( Guid postId,string userId);
    }
}
