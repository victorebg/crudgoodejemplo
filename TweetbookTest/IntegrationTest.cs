
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Tweetbook.Contracts.V1;
using Tweetbook.Contracts.V1.Requests;
using Tweetbook.Contracts.V1.Responses;
using Tweetbook.Data;

namespace Tweetbook.IntegrationTest
{
    public class IntegrationTest
    {
        protected readonly HttpClient TestClient;

        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    //In real ecenarios this wuold be weith a real database....
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveAll(typeof(DataContext));
                        services.AddDbContext<DataContext>(options =>
                        {
                            options.UseInMemoryDatabase("TestDb");

                        });

                    });
                });

            TestClient = appFactory.CreateClient();

        }

        protected async Task AuthenticateAsync()
        {
            TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtStringAsync());
        }

        protected async Task<string> GetJwtStringAsync()
        {
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Identity.Registro, new CreateIdentityRequest
            {
                Email = "solenny@gmail.com",
                Password = "BritoPro061!"

            });

            var registrationResponseObject = await response.Content.ReadAsAsync<AuthSuccessResponse>();
            return registrationResponseObject.Token;
        }

        protected async Task<CreatePostResponse> CreatePostAsync(CreatePostRequest createPostRequest)
        {
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Posts.Create,createPostRequest);
            return await response.Content.ReadAsAsync<CreatePostResponse>();
        }
    }
}
